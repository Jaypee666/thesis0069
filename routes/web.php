<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/','LoginController@getLogin');
Route::get('/login','LoginController@getLogin');
Route::post('/login','LoginController@postLogin');
Route::post('/logout','LoginController@doLogout');

Route::get('/admin','AdminController@getIndex')->middleware('admin');

Route::get('/admin/teachers','AdminController@getTeachers')->middleware('admin');
Route::post('/admin/teachers/add','AdminController@storeTeachers')->middleware('admin');
Route::get('/admin/teachers/edit/{id}','AdminController@editTeachers')->middleware('admin');
Route::post('/admin/teachers/update','AdminController@updateTeachers')->middleware('admin');
Route::post('/admin/teachers/import','AdminController@importTeachers')->middleware('admin');

Route::get('/admin/users','AdminController@getUsers')->middleware('admin');
Route::post('/admin/users/add','AdminController@storeUsers')->middleware('admin');
Route::get('/admin/users/edit/{id}','AdminController@editUsers')->middleware('admin');
Route::post('/admin/users/update','AdminController@updateUsers')->middleware('admin');
Route::post('/admin/users/import','AdminController@importUsers')->middleware('admin');

Route::get('/admin/classrooms','AdminController@getClassrooms')->middleware('admin');
Route::post('/admin/classrooms/add','AdminController@storeClassrooms')->middleware('admin');
Route::get('/admin/classrooms/edit/{id}','AdminController@editClassrooms')->middleware('admin');
Route::post('/admin/classrooms/update','AdminController@updateClassrooms')->middleware('admin');
Route::post('/admin/classrooms/import','AdminController@importClassrooms')->middleware('admin');

Route::get('/admin/students','AdminController@getStudents')->middleware('admin');
Route::post('/admin/students/add','AdminController@storeStudents')->middleware('admin');
Route::get('/admin/students/edit/{id}','AdminController@editStudents')->middleware('admin');
Route::post('/admin/students/update','AdminController@updateStudents')->middleware('admin');
Route::post('/admin/students/import','AdminController@importStudents')->middleware('admin');


Route::get('/admin/faces/images', 'AdminController@facePath')->middleware('admin');
Route::get('/admin/faces', 'AdminController@getFaces')->middleware('admin');
Route::get('/admin/faces/train', 'AdminController@getTrainFaceRecognition')->middleware('admin');
Route::post('/admin/faces/register','AdminController@postRegisterFace')->middleware('admin');
Route::post('/admin/faces/import','AdminController@importFaces')->middleware('admin');

Route::get('/admin/userlogs', 'AdminController@getUserLogs');
Route::post('/admin/userlogs/userlog.pdf', 'AdminController@postGenerateUserLogs');
Route::get('/admin/suspension', 'AdminController@getSuspensionList');
Route::post('/admin/suspension/add', 'AdminController@postAddSuspension');

Route::get('/admin/sms', 'AdminController@getSmsList');
Route::get('/admin/sms/edit', 'AdminController@getEditSms');
Route::post('/admin/sms/edit', 'AdminController@updateSms');


Route::get('/admin/ajax/facedetect','AdminController@ajaxFaceDetect' )->middleware('admin');

Route::get('/moderator','ModeratorController@getIndex')->middleware('moderator');

Route::get('/moderator/requests','ModeratorController@getRequests')->middleware('moderator');
Route::post('/moderator/requests/attendance/update','ModeratorController@updateRequests')->middleware('moderator');
Route::get('/moderator/reports','ModeratorController@getReports')->middleware('moderator');
Route::get('/moderator/reports/StudentProfile','ModeratorController@getStudentReport')->middleware('moderator');
Route::get('/moderator/reports/DailyAttendance','ModeratorController@getStudentReport')->middleware('moderator');
Route::post('/moderator/reports/ClassAttendance','ModeratorController@postClassReport')->middleware('moderator');
Route::post('/moderator/reports/AttendanceSummary','ModeratorController@postAttendanceReport')->middleware('moderator');
Route::post('/moderator/reports/AppealSummary','ModeratorController@postAppealReport')->middleware('moderator');


Route::get('/moderator/attendance','ModeratorController@getAttendance')->middleware('moderator');
Route::post('/moderator/attendance/add','ModeratorController@storeAttendance')->middleware('moderator');
Route::get('/moderator/attendance/edit/{id}','ModeratorController@editAttendance')->middleware('moderator');
Route::post('/moderator/attendance/update','ModeratorController@updateAttendance')->middleware('moderator');


Route::get('/teacher','TeacherController@getIndex')->middleware('teacher');
Route::get('/teacher/attendance','TeacherController@getAttendance')->middleware('teacher');
//Route::post('/teacher/attendance/take','TeacherController@getHardcodeAttendanceTake')->middleware('teacher');
Route::post('/teacher/attendance/take','TeacherController@getAttendanceTake')->middleware('teacher');
Route::get('/teacher/requests','TeacherController@getRequests')->middleware('teacher');
Route::post('/teacher/requests/add','TeacherController@storeRequests')->middleware('teacher');
Route::get('/teacher/requests/edit/{id}','TeacherController@editRequests')->middleware('teacher');
Route::post('/teacher/requests/update','TeacherController@updateRequests')->middleware('teacher');
Route::post('/teacher/requests/delete','TeacherController@deleteRequests')->middleware('teacher');
Route::get('/teacher/class/attendance','TeacherController@getClassAttendance')->middleware('teacher');






