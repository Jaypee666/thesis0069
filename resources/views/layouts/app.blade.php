
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'De La Salle Lipa Attendance Monitoring System') }}</title>
        <!-- Styles -->
        
        <link href="/css/app.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">
        <!-- Scripts -->
        <script>
        window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
        ]); ?>
        </script>
        <link href="/js/jquery-ui/jquery-ui.css" rel="stylesheet">  
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}">
                            {{ config('app.name', 'De La Salle Lipa Attendance Monitoring System') }}
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>
                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->firstname }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                    
                        </ul>
                    </div>
                    @if(Auth::user()->userlevel_id == 1)
                    <div class="navbar">
                        <ul class="nav navbar-nav">
                            <li><a href="{{ url('admin') }}">Home</a></li>
                            <li><a href="{{ url('admin/users') }}">Manage Users</a></li>
                            <li><a href="{{ url('admin/teachers') }}">Manage Teachers</a></li>
                            <li><a href="{{ url('admin/classrooms') }}">Manage Classrooms</a></li>
                            <li><a href="{{ url('admin/students') }}">Manage Students</a></li>
                            <li><a href="{{ url('admin/faces') }}">Manage Faces</a></li>
                            <li><a href="{{ url('admin/userlogs') }}">User Logs</a></li>
                            <li><a href="{{ url('admin/sms') }}">Manage SMS</a></li>
<!--                             <li><a href="{{ url('admin/suspension') }}">Suspended Dates</a></li> -->
                        </ul>
                    </div>
                    @endif
                     @if(Auth::user()->userlevel_id == 2)
                    <div class="navbar">
                        <ul class="nav navbar-nav">
                            <li><a href="{{ url('moderator') }}">Home</a></li>
                            <li><a href="{{ url('moderator/requests') }}">Manage Requests</a></li>
                            <li><a href="{{ url('moderator/attendance') }}">Manage Attendance</a></li>
                            <li><a href="{{ url('moderator/reports') }}">Manage Reports</a></li>
                        </ul>
                    </div>
                    @endif
                    @if(Auth::user()->userlevel_id == 3)
                    <div class="navbar">
                        <ul class="nav navbar-nav">
                            <li><a href="{{ url('teacher') }}">Home</a></li>
                            <li><a href="{{ url('teacher/attendance') }}">Take Attendance</a></li>
                            <li><a href="{{ url('teacher/requests') }}">Manage Requests</a></li>
                            <li><a href="{{ url('teacher/class/attendance') }}">Class Attendance</a></li>
                        </ul>
                    </div>
                    @endif
                    @endif
                </div>
            </nav>
            @yield('content')
        </div>
        <!-- Scripts -->
        <script src="/js/app.js"></script>
        <script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="/js/jquery-ui/jquery-ui.min.js"></script>
        @yield('script')
    </body>
</html>