@extends('layouts.app')
@section('content')
<div class="container">
	<p>Attendance Appeal</p>
	<div class="row">
		<div class="col-md-12">
			<form role="form" method="POST" action="{{url('/teacher/requests/update')}}">
				<div class="form-group">
					<label for="name">Student:</label>
					<select class="form-control" name="student_id" id="name">
						@foreach($students as $student)
						<option value=" {{ $student->id }}"{{ $teacherRequest->student->id==$student->id? 'selected':null}}>{{ $student->fname." ".$student->mname." ".$student->lname }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="attendance_date">Attendance Date:</label>
					<input type="date" name="attendance_date" value="{{ $teacherRequest->attendance_date }}">
				</div>
				<div class="form-group">
					<label for="attendance_date">Attendance Date:</label>
					<input type="time" name="attendance_time" value="{{ $teacherRequest->attendance_time }}">
				</div>
				<div class="form-group">
					<label for="name">Remarks:</label>
					<select class="form-control" name="remarks" id="myvalue" onchange="teacherConnect()">
						@if($teacherRequest->remarks == "Present")
						<option value="Present" selected>Present</option>
						<option value="Absent">Absent</option>
						@elseif($teacherRequest->remarks == "Absent")
						<option value="Present">Present</option>
						<option value="Absent" selected>Absent</option>
						@endif
					</select>
				</div>
				<div class="form-group">
					<label for="name">Description:</label>
					<select class="form-control" name="description" id="mySelect">
						<option value="None"></option>
						<option value="Tardy">Tardy</option>
					</select>
				</div>
				<div class="form-group">
					<label for="name">Status:</label>
					<select class="form-control" name="status" id="name">
						@if($teacherRequest->status == 1)
						<option value="1">Active</option>
						@else
						<option value="0">Inactive</option>
						@endif
					</select>
				</div>
				<input type="hidden" value="{{$teacherRequest->id}}" name="teacherRequest_id">
				<input type="hidden" value="{{ Auth::user()->teacher_id }}" name="teacher_id">
				<input type="hidden" value="{{Session::token()}}" name="_token">
				<button type="submit" class="btn btn-default">Add Request</button>
			</form>
		</div>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	function teacherConnect()
	{
		var selected = $("#myvalue").val();
		console.log(selected);
		if(selected=="Present")
		{
			$("#mySelect").empty();
			$("#mySelect").append('<option value="None"></option>');
			$("#mySelect").append('<option value="Tardy">Tardy</option>');
		}
		else
		{
			$("#mySelect").empty();
			$("#mySelect").append('<option value="Cutting" selected>Cutting</option>');
			$("#mySelect").append('<option value="Not Excused">Not Excused</option>');
			$("#mySelect").append('<option value="Excused">Excused</option>');
		}
	}
</script>


