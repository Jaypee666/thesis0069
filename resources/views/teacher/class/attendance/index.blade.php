@extends('layouts.app')
@section('content')
<div class="container">
	<p>Attendance Appeal</p>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label for="name">Classroom:</label>
				<select class="form-control" name="classroom" id="classroom">
					@foreach($classrooms as $classroom)
					<option value="{{ $classroom->id}}">{{ $classroom->class_name }}</option>
					@endforeach
				</select>
			</div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Student No.</th>
						<th>Student Name</th>
						<th>Remarks</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
						@foreach($classAttendances as $classAttendance)
						<tr>
							<td>
								{{ $classAttendance->student_number }}
							</td>
							<td>
								{{ $classAttendance->fname." ".$classAttendance->mname." ".$classAttendance->lname }}
							</td>
							<td>
								{{ $classAttendance->remarks }}
							</td>
							<td>
								{{ $classAttendance->attendance_date." ".$classAttendance->attendance_time }}
							</td>
						</tr>
						@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('change', '#classroom', function(){

			var c_id=$(this).val();
			console.log(c_id);

			$.ajax({
				type:'get',
				url:'{!!URL::to('findattendance')!!}',
				data:{'id':c_id},
				sucess:function(data){
					console.log('sucess');
					console.log(data);
				},
				error:function(){
					console.log('error');
				}
			});
		});
	});
</script>
@endsection