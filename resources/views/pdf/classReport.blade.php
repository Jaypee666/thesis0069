<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="/css/app.css" rel="stylesheet">
		<title></title>
		<style>
			div.table{
				margin: 20px 0px 0px 0px;
			}
			div.title{
				padding-bottom: 10px;
				text-align: center;
			}
			table {
				border-collapse: collapse;
				width: 100%;
				text-align: center;
			}
			table, th, td {
				border: 1px solid black;
			}
			td {
				padding: 0px 5px;
			}
			p {
				margin: 0px;
			}
			h3.center {
				padding-top: 10px;
				text-align: center;
			}
			img {
				padding: 0px;
			}
		</style>
	</head>
	<body>
		<div class="title">
			<p>
				<img src="images/logo.png" height="30px">
				De La Salle Lipa Integrated School
			</p>
			<p>Attendance Report for {{$data["classroom"]}} for {{$data["from"]}} to {{$data["to"]}}</p>
		</div>
		<div>
			<h3 class="center">Class Attendance of {{$data["classroom"]}}</h3>
			<table border="1">
				<thead>
					<tr>
						<th>#</th>
						<th>Student</th>
						<th>Attendance</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data["logs"] as $key => $log)
					<tr>
						<td>
							{{ $key+1 }}
						</td>
						<td>
							{{ $log->student->fname." ".$log->student->mname." ".$log->student->lname }}
						</td>
						<td>
							{{ $log->remarks." ".$log->description }}
						</td>
						<td>
							{{ $log->attendance_date." ".$log->attendance_time }}
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
	</body>
</html>

