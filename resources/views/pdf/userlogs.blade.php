<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="/css/app.css" rel="stylesheet">
		<title></title>
		<style>
			div.table{
				margin: 20px 0px 0px 0px;
			}
			div.title{
				padding-bottom: 10px;
				text-align: center;
			}
			table {
				border-collapse: collapse;
				width: 100%;
			}
			table, th, td {
				border: 1px solid black;
			}
			td {
				padding: 0px 5px;
			}
			p {
				margin: 0px;
			}
			img {
				padding: 0px;
			}
		</style>
	</head>
	<body>
		<div class="title">
			<p>
				<img src="images/logo.png" height="30px">
				De La Salle Lipa Integrated School
			</p>
			<p>User log Report</p>
		</div>
		<div class="table">
			<table border="1">
				<thead>
					<tr>
						<th>#</th>
						<th>User Name</th>
						<th>User Level</th>
						<th>Date Logged In</th>
					</tr>
				</thead>
				<tbody>
					@foreach($logs as $key => $log)
					<tr>
						<td style="text-align: center;"> {{ $key+1 }} </td>
						<td> {{ $log->user->firstname." ".$log->user->middlename." ".$log->user->lastname }} </td>
						<td> {{ $log->user->userlevel->level_name }}</td>
						<td> {{ $log->created_at }} </td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</body>
</html>

