<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="/css/app.css" rel="stylesheet">
		<title></title>
		<style>
			div.table{
				margin: 20px 0px 0px 0px;
			}
			div.title{
				padding-bottom: 10px;
				text-align: center;
			}
			table {
				border-collapse: collapse;
				width: 100%;
				text-align: center;
			}
			table, th, td {
				border: 1px solid black;
			}
			td {
				padding: 0px 5px;
			}
			p {
				margin: 0px;
			}
			h3.center {
				padding-top: 10px;
				text-align: center;
			}
			img {
				padding: 0px;
			}
		</style>
	</head>
	<body>
		<div class="title">
			<p>
				<img src="images/logo.png" height="30px">
				De La Salle Lipa Integrated School
			</p>
			<p>Student Report - <b>{{$data['student_info']->fname}} {{$data['student_info']->mname}} {{$data['student_info']->lname}}</b></p>
		</div>
		<div>
			<h3 class="center">Absent Without Excuse</h3>
			<table border="1">
				<thead>
					<tr>
						<th>#</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					@if($data["not"] == null)
					<tr>
						<td>
							
						</td>
						<td>
							
						</td>
					</tr>
					@else
					@foreach($data["not"] as $key => $not)
					<tr>
						<td>
							{{ $key+1 }}
						</td>
						<td>
							{{ $not->attendance_date }}
						</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
			<h3 class="center">Excused Absence</h3>
			<table border="1">
				<thead>
					<tr>
						<th>#</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					@if($data["excused"] == null)
					<tr>
						<td>
							
						</td>
						<td>
							
						</td>
					</tr>
					@else
					@foreach($data["excused"] as $key => $excused)
					<tr>
						<td>
							{{ $key+1 }}
						</td>
						<td>
							{{ $excused->attendance_date }}
						</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
			<h3 class="center">Tardy</h3>
			<table border="1">
				<thead>
					<tr>
						<th>#</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					@if($data["tardy"] == null)
					<tr>
						<td>
							
						</td>
						<td>
							
						</td>
					</tr>
					@else
					@foreach($data["tardy"] as $key => $tardy)
					<tr>
						<td>
							{{ $key+1 }}
						</td>
						<td>
							{{ $tardy->attendance_date }}
						</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
			<h3>Total number of attendance: {{$data["numberOfClasses"]}}</h3>
			<h3>Present: {{count($data["present"])}}</h3>
			<h3>Absent: {{count($data["absent"])}}</h3>
		</div>
	</body>
</html>