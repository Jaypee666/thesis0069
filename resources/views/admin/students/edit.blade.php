@extends('layouts.app')
@section('content')
<div class="container">
	<p>Manage Classroom</p>
	<div class="row">
		<form role="form" method="POST" action="{{url('/admin/students/update')}}">
			<div class="form-group">
				<label for="name">Student's First Name:</label>
				<input type="text" name="student_fname" class="form-control" id="name" placeholder="Enter First Name" value="{{ $student->fname }}">
			</div>
			<div class="form-group">
				<label for="name">Student's Last Name:</label>
				<input type="text" name="student_lname" class="form-control" id="name" placeholder="Enter Last Name" value="{{ $student->lname }}">
			</div>
			<div class="form-group">
				<label for="name">Student's Middle Name:</label>
				<input type="text" name="student_mname" class="form-control" id="name" placeholder="Enter Middle Name" value="{{ $student->mname }}">
			</div>
			<div class="form-group">
				<label for="name">Year:</label>
				<input type="text" name="year" class="form-control" id="name" placeholder="Enter Year" value="{{ $student->year }}">
			</div>
			<div class="form-group">
				<label for="name">Sex:</label>
				<select class="form-control" name="sex" id="name">
				@if($student->sex == "Male")
					<option value="Male" selected>Male</option>
					<option value="Female">Female</option>
				@elseif($student->sex == "Female")
					<option value="Male">Male</option>
					<option value="Female" selected>Female</option>
				@endif
				</select>
			</div>
			<div class="form-group">
				<label for="name">Classroom to be assigned:</label>
				<select class="form-control" name="class_id" id="name">
					@foreach($classrooms as $classroom)
					<option value=" {{ $classroom->id }} " {{ $student->classroom->id==$classroom->id ? 'selected':null }}>{{ $classroom->class_name }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
					<label for="name">Guardian's First Name:</label>
					<input type="text" name="guardian_fname" class="form-control" id="name" placeholder="Enter First Name" value="{{ $student->guardian_fname }}">
				</div>
				<div class="form-group">
					<label for="name">Guardian's Last Name:</label>
					<input type="text" name="guardian_lname" class="form-control" id="name" placeholder="Enter Last Name" value="{{ $student->guardian_lname }}">
				</div>
				<div class="form-group">
					<label for="name">Guardian's Middle Name:</label>
					<input type="text" name="guardian_mname" class="form-control" id="name" placeholder="Enter Middle Name" value="{{ $student->guardian_mname }}">
				</div>
				<div class="form-group">
					<label for="name">Guardian's Mobile Number:</label>
					<input type="text" name="guardian_mobile" class="form-control" id="name" placeholder="Enter Mobile Number" value="{{ $student->guardian_mobile }}">
				</div>
			<div class="form-group">
				<label for="name">Status:</label>
				<select class="form-control" name="status" id="name">
				@if($student->status == 1)
					<option value="1">Active</option>
				@else
					<option value="0">Inactive</option>
				@endif
				</select>
			</div>
			<input type="hidden" value="{{ $student->id }}" name="student_id">
			<input type="hidden" value="{{Session::token()}}" name="_token">
			<button type="submit" class="btn btn-default">Edut Student</button>
		</form>
	</div>
</div>
@endsection