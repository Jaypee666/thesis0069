@extends('layouts.app')
@section('content')
<div class="container">
	<p>Manage Students</p>
	<div class="row">
		@if(Session::has('success'))
		<div class="alert alert-success">
			<strong>Success: </strong> {{ Session::get('success') }}
		</div>
		@elseif(Session::has('warning'))
		<div class="alert alert-warning">
			<strong>Warning: </strong> {{ Session::get('warning') }}
		</div>
		@endif
		@if(count($errors) > 0)
		<div class="alert alert-warning">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-md-2">
			<form role="form" method="POST" action="{{url('/admin/students/add')}}">
				<div class="form-group">
					<label for="student_number">Student Number:</label>
					<input type="text" name="student_number" class="form-control" id="name" placeholder="Enter Student Number" value="{{ Request::old('student_number')}}">
				</div>
				<div class="form-group">
					<label for="name">Student's First Name:</label>
					<input type="text" name="student_fname" class="form-control" id="name" placeholder="Enter First Name" value="{{ Request::old('student_fname')}}">
				</div>
				<div class="form-group">
					<label for="name">Student's Last Name:</label>
					<input type="text" name="student_lname" class="form-control" id="name" placeholder="Enter Last Name" value="{{ Request::old('student_lname')}}">
				</div>
				<div class="form-group">
					<label for="name">Student's Middle Name:</label>
					<input type="text" name="student_mname" class="form-control" id="name" placeholder="Enter Middle Name" value="{{ Request::old('student_mname')}}">
				</div>
				<div class="form-group">
					<label for="name">Year:</label>
					<input type="text" name="year" class="form-control" id="name" placeholder="Enter Year" value="{{ Request::old('year')}}">
				</div>
				<div class="form-group">
					<label for="name">Sex:</label>
					<select class="form-control" name="sex" id="name">
						<option value="Male">Male</option>
						<option value="Female">Female</option>
					</select>
				</div>
				<div class="form-group">
					<label for="name">Classroom to be assigned:</label>
					<select class="form-control" name="class_id" id="name">
						@foreach($classrooms as $classroom)
						<option value=" {{ $classroom->id }}">{{ $classroom->class_name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="name">Guardian's First Name:</label>
					<input type="text" name="guardian_fname" class="form-control" id="name" placeholder="Enter First Name" value="{{ Request::old('guardian_fname')}}">
				</div>
				<div class="form-group">
					<label for="name">Guardian's Last Name:</label>
					<input type="text" name="guardian_lname" class="form-control" id="name" placeholder="Enter Last Name" value="{{ Request::old('guardian_lname')}}">
				</div>
				<div class="form-group">
					<label for="name">Guardian's Middle Name:</label>
					<input type="text" name="guardian_mname" class="form-control" id="name" placeholder="Enter Middle Name" value="{{ Request::old('guardian_mname')}}">
				</div>
				<div class="form-group">
					<label for="name">Guardian's Mobile Number:</label>
					<input type="text" name="guardian_mobile" class="form-control" id="name" placeholder="Enter Mobile Number" value="{{ Request::old('guardian_mobile')}}">
				</div>
				<input type="hidden" value="1" name="status">
				<input type="hidden" value="{{Session::token()}}" name="_token">
				<button type="submit" class="btn btn-default">Add Student</button>
			</form>
			<form method="POST" action="{{url('/admin/students/import')}}" enctype="multipart/form-data">
				<div class="form-group">
						<input type="hidden" value="{{Session::token()}}" name="_token">
						<label for="import">Import Data(Optional):</label>
						<input type="file" class="btn btn-default" name="import" id="import" onchange="form.submit()">
				</div>
			</form>
		</div>
		<div class="col-md-10">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Student ID</th>
						<th>Student Number</th>
						<th>Student Name</th>
						<th>Student Year</th>
						<th>Student Sex</th>
						<th>Student Classroom</th>
						<th>Guardian Name</th>
						<th>Guardian Mobile Number</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($students as $student)
					<tr>
						<td>
							{{ $student->id }}
						</td>
						<td>
							{{ $student->student_number }}
						</td>
						<td>
							{{ $student->fname." ".$student->mname." ".$student->lname }}
						</td>
						<td>
							{{ $student->year }}
						</td>
						<td>
							{{ $student->sex }}
						</td>
						<td>
							{{ $student->classroom->class_name }}
						</td>
						<td>
							{{ $student->guardian_fname." ".$student->guardian_mname." ".$student->guardian_lname }}
						</td>
						<td>
							{{ $student->guardian_mobile }}
						</td>
						<td>
							<button type="button" class=".btn-danger"> <a href="{{ url('admin/students/edit', ['id'=>$student->id]) }}">Edit</a></button>
							<button type="button" class=".btn-danger"> <a href="">Delete</a> </button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $students->links() }}
		</div>
	</div>
</div>
@endsection