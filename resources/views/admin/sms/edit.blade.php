@extends('layouts.app')
@section('content')
<div class="container">
	<p>Manage Classroom</p>
	<div class="row">
		<form role="form" method="POST" action="{{url('/admin/sms/edit')}}">
			<div class="form-group">
				<label for="type">SMS type:</label>
				<input type="text" name="type" class="form-control" id="type" placeholder="Enter First Name" value="{{ $sms->type }}" readonly>
			</div>
			<div class="form-group">
				<label for="content">Content:</label>
				<input type="text" name="content" class="form-control" id="content" placeholder="Enter First Name" value="{{ $sms->content }}">
			</div>
			<input type="hidden" value="{{ $sms->id }}" name="id">
			<input type="hidden" value="{{Session::token()}}" name="_token">
			<button type="submit" class="btn btn-default">Edit Student</button>
		</form>
	</div>
</div>
@endsection