@extends('layouts.app')
@section('content')
<div class="container">
<p>Suspended Dates</p>
	<div class="row">
	@if(Session::has('success'))
		<div class="alert alert-success">
			<strong>Success: </strong> {{ Session::get('success') }}
		</div>
	@endif
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>Type</th>
						<th>Content</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($sms as $message)
					<tr>
						<td>{{$message->id}}</td>
						<td>{{$message->type}}</td>
						<td>{{$message->content}}</td>
						<td><a class="btn" href="{{url('admin/sms/edit?id='.$message->id)}}">Edit</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
	</div>
</div>

@endsection

@section('script')

@endsection