@extends('layouts.app')
@section('content')
<div class="container">
	<p>Manage Classrooms</p>
	<div class="row">
		@if(Session::has('success'))
		<div class="alert alert-success">
			<strong>Success: </strong> {{ Session::get('success') }}
		</div>
		@elseif(Session::has('warning'))
		<div class="alert alert-warning">
			<strong>Warning: </strong> {{ Session::get('warning') }}
		</div>
		@endif
		@if(count($errors) > 0)
		<div class="alert alert-warning">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-md-4">
			<form role="form" method="POST" action="{{url('/admin/classrooms/add')}}">
				<div class="form-group">
					<label for="name">Classroom Name:</label>
					<input type="text" name="classroom_name" class="form-control" id="name" placeholder="Enter Classroom Name" value="{{ Request::old('classroom_name')}}">
				</div>
				<div class="form-group">
					<label for="name">Teacher to be assigned:</label>
					<select class="form-control" name="teacher_id" id="name">
						@foreach($teachers as $teacher)
						<option value=" {{ $teacher->id }}">{{ $teacher->fname." ".$teacher->mname." ".$teacher->lname }}</option>
						@endforeach
					</select>
				</div>
				<input type="hidden" value="{{Session::token()}}" name="_token">
				<input type="hidden" value="1" name="status">
				<button type="submit" class="btn btn-default">Add Classroom</button>
			</form>
			<form method="POST" action="{{url('/admin/classrooms/import')}}" enctype="multipart/form-data">
				<input type="hidden" value="{{Session::token()}}" name="_token">
				<div class="form-group">
						<label for="import">Import Data(Optional):</label>
						<input type="file" class="btn btn-default" name="import" id="import" onchange="form.submit();">
				</div>
			</form>
		</div>
		<div class="col-md-8">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Classroom ID</th>
						<th>Classroom Name</th>
						<th>Classroom Adviser</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($classrooms as $classroom)
					<tr>
						<td>
							{{ $classroom->id }}
						</td>
						<td>
							{{ $classroom->class_name }}
						</td>
						<td>
							{{ $classroom->teacher->fname." ".$classroom->teacher->mname." ".$classroom->teacher->lname }}
						</td>
						<td>
							<button type="button" class=".btn-danger"> <a href="{{ url('admin/classrooms/edit', ['id'=>$classroom->id]) }}">Edit</a></button>
							<button type="button" class=".btn-danger"> <a href="">Delete</a> </button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $classrooms->links() }}
		</div>
	</div>
</div>
@endsection