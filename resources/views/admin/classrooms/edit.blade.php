@extends('layouts.app')
@section('content')
<div class="container">
	<p>Manage Classroom</p>
	<div class="row">
		<form role="form" method="POST" action="{{url('/admin/classrooms/update')}}">
			<div class="form-group">
				<label for="name">Classroom Name:</label>
				<input type="text" name="classroom_name" class="form-control" id="name" placeholder="Enter Classroom Name" value="{{ $classroom->class_name }}">
			</div>
			<div class="form-group">
				<label for="name">Teacher to be assigned:</label>
				<select class="form-control" name="teacher_id" id="name">
					@foreach($teachers as $teacher)
					<option value="{{ $teacher->id }}" {{ $classroom->teacher->id==$teacher->id ? 'selected':null }} >{{ $teacher->fname." ".$teacher->mname." ".$teacher->lname }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label for="name">Status:</label>
				<select class="form-control" name="status" id="name">
				@if($classroom->status == 1)
					<option value="1">Active</option>
				@else
					<option value="0">Inactive</option>
				@endif
				</select>
			</div>
			<input type="hidden" value="{{ $classroom->id }}" name="classroom_id">
			<input type="hidden" value="{{Session::token()}}" name="_token">
			<button type="submit" class="btn btn-default">Edit Classroom</button>
		</form>
	</div>
</div>
@endsection