@extends('layouts.app')
@section('content')
<div class="container">
<p>User Log Reports</p>
	<div class="row">
		<div class="col-md-4">
			<form role="form" method="POST" action="{{url('/admin/userlogs/userlog.pdf')}}">
				<div class="form-group">
					<label for="dateFrom">From:</label>
					<input type="text" name="dateFrom" class="form-control" id="dateFrom">
				</div>
				<div class="form-group">
					<label for="dateFrom">To:</label>
					<input type="text" name="dateTo" class="form-control" id="dateTo">
				</div>
				<input type="hidden" value="1" name="status">
				<input type="hidden" value="{{Session::token()}}" name="_token">
				<button type="submit" class="btn btn-default">Add User</button>
			</form>
		</div>
		<div class="col-md-8">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Timestamp</th>
						<th>Username</th>
						<th>Type</th>
					</tr>
				</thead>
				<tbody>
					@foreach($logs as $log)
					<tr>
						<td>{{$log->created_at}}</td>
						<td>{{$log->user->firstname}} {{$log->user->middlename}} {{$log->user->lastname}}</td>
						<td>{{$log->user->userlevel->level_name}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $logs->links() }}
		</div>
	</div>
</div>

@endsection

@section('script')
<script>
$( function() {
	$("#dateFrom").datepicker();
	$("#dateTo").datepicker();
});
</script>
@endsection