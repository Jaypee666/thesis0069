@extends('layouts.app')
@section('content')
<div class="container">
<p>Manage Faces</p>
	<div class="row">
		<div class="col-md-4">
            <form method="POST" action="{{ url('admin/faces/import')}}" class="dropzone" id="imageDropzone" style="background-color:white; font-size: 20px">
                <input type="hidden" value="{{Session::token()}}" name="_token">
            </form>
		</div>
		<div class="col-md-8">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <strong>Success: </strong> {{ Session::get('success') }}
            </div>
        @endif
			<button type="button" class=".btn-danger"> <a href="{{ url('admin/faces/train')}}" class=".btn-danger">Train</a></button>
			<button type="button" class=".btn-danger" id="faceDetect">Face Detect</button>
			<form role="form" method="POST" action="{{url('/admin/faces/register')}}">
				<div id="faces">
				</div>
				<div class="form-group">
					<label for="studId">Student Number:</label>
					<input type="text" name="studId" class="form-control" id="studId" placeholder="Enter Student Number">
					<input type="submit">
					<input type="hidden" value="{{Session::token()}}" name="_token">
				</div>
			</form>
		</div>
	</div>
    <div class="row">
        <table class="table table-bordered">
            <thead>
                <tr>
                   <th>User ID</th>
                   <th>Face</th>
                   <th>Student Number</th>
                   <th>Full Name</th>
                </tr>
            </thead>
            <tbody>
            @foreach($students as $student)
                <tr>
                    <td>{{$student->id}}</td>
                    <td><img src="{{url('/admin/faces/images?id='.$student->id.'')}}" height="200px" width="200px"></td>
                    <td>{{$student->student_number}}</td>
                    <td>{{$student->fname}} {{$student->mname}} {{$student->lname}}</td>
                </tr>
            @endforeach
            </tbody> 
        </table>
        {{ $students->links() }}
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="/js/dropzone.js"></script>
<script>

$("button").click(faceDetect);

    function faceDetect(params) {
        $.ajax({
            url: "{{url('/admin/ajax/facedetect')}}",
            success:function(data){
                console.log(data);
            	if(data['count']>0)
            	{
            		$('#faces').html('<h1>Detected: '+data['count']+'</h1>');
                	$('#faces').append("<img src='data:image/png;base64,"+data['image']+"' id='originalPicture' height='200' width='300'/>");
                	var image = document.getElementById("originalPicture");
                    var i = 0;
                    console.log(i);
            		$('#faces').append("<canvas id='face"+i+"' hidden>");
            		var canvas = document.getElementById("face"+i);
            		canvas.width = parseInt(data['points'][i]['w']);
            		canvas.height = parseInt(data['points'][i]['h']);
            		//canvas.width = 200;
                    //canvas.height = 300;
                    ctx = canvas.getContext('2d');
            		ctx.drawImage(image,parseInt(data['points'][i]['x']),parseInt(data['points'][i]['y']),parseInt(data['points'][i]['w']),parseInt(data['points'][i]['h']),0,0,parseInt(data['points'][i]['w']),parseInt(data['points'][i]['h']));
                    ctx.drawImage(image,data['points'][i]['x'],data['points'][i]['y']);
            		var data = document.getElementById("face"+i);
            		var dataUrl = data.toDataURL();
            		$('#faces').append("<input type='radio' name='image' value='"+dataUrl+"' checked>");
            	}
            	else
            	{
            		$('#faces').html('<h1>No face detected</h1>');
            	}
            },
            dataType:"JSON"
        });
    }
</script>

@endsection