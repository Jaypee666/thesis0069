@extends('layouts.app')
@section('content')
<div class="container">
	<p>Manage Users</p>
	<div class="row">
		@if(Session::has('success'))
		<div class="alert alert-success">
			<strong>Success: </strong> {{ Session::get('success') }}
		</div>
		@elseif(Session::has('warning'))
		<div class="alert alert-warning">
			<strong>Warning: </strong> {{ Session::get('warning') }}
		</div>
		@endif
		@if(count($errors) > 0)
		<div class="alert alert-warning">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-md-4">
			<form role="form" method="POST" action="{{url('/admin/users/add')}}">
				<div class="form-group">
					<label for="name">Username:</label>
					<input type="text" name="user_username" class="form-control" id="name" placeholder="Enter Username" value="{{ Request::old('user_username')}}">
				</div>
				<div class="form-group">
					<label for="name">Password:</label>
					<input type="password" name="user_password" class="form-control" id="name" placeholder="Enter Password">
				</div>
				<div class="form-group">
					<label for="name">First Name:</label>
					<input type="text" name="user_fname" class="form-control" id="name" placeholder="Enter First Name" value="{{ Request::old('user_fname')}}">
				</div>
				<div class="form-group">
					<label for="name">Middle Name:</label>
					<input type="text" name="user_mname" class="form-control" id="name" placeholder="Enter Middle Name" value="{{ Request::old('user_mname')}}">
				</div>
				<div class="form-group">
					<label for="name">Last Name:</label>
					<input type="text" name="user_lname" class="form-control" id="name" placeholder="Enter Last Name" value="{{ Request::old('user_lname')}}">
				</div>
				<div class="form-group">
					<label for="user_userlvl">User Level:</label>
					<select class="form-control" name="user_userlvl" id="user_userlvl" onchange="teacherConnect()">
						@foreach($userlevels as $userlevel)
						<option value=" {{ $userlevel->id }}">{{ $userlevel->level_name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group" id="teacherConnectInput">
					<label for="name">Teacher to be assigned:</label>
					<select class="form-control" name="teacher_id" id="teacher_id">
						<option value="null"> None </option>
						@foreach($teachers as $teacher)
						<option value=" {{ $teacher->id }}">{{ $teacher->fname." ".$teacher->mname." ".$teacher->lname }}</option>
						@endforeach
					</select>
				</div>
				<input type="hidden" value="1" name="status">
				<input type="hidden" value="{{Session::token()}}" name="_token">
				
				
				<button type="submit" class="btn btn-default">Add User</button>

			</form>
			<form method="POST" action="{{url('/admin/users/import')}}" enctype="multipart/form-data">
				<div class="form-group">
						<input type="hidden" value="{{Session::token()}}" name="_token">
						<label for="import">Import Data(Optional):</label>
						<input type="file" class="btn btn-default" name="import" id="import" onchange="form.submit()">
				</div>
			</form>
		</div>
		<div class="col-md-8">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>User ID</th>
						<th>Username</th>
						<th>Full Name</th>
						<th>Status</th>
						<th>User Level</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td>
							{{ $user->id }}
						</td>
						<td>
							{{ $user->username}}
						</td>
						<td>
							{{ $user->firstname." ".$user->middlename." ".$user->lastname }}
						</td>
						<td>
							{{ $user->status }}
						</td>
						<td>
							{{ $user->userlevel->level_name }}
						</td>
						<td>
							<button type="button" class=".btn-danger"> <a href="{{ url('admin/users/edit', ['id'=>$user->id]) }}">Edit</a></button>
							<button type="button" class=".btn-danger"> Delete </button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $users->links() }}
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$("#teacherConnectInput").hide();
	function teacherConnect()
	{
		var selected = $("#user_userlvl").val();
		console.log(selected);
		if(selected==3)
		{
			$("#teacherConnectInput").show();
		}
		else
		{
			$("#teacherConnectInput").hide();
		}
	}
</script>
@endsection