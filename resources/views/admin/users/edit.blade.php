@extends('layouts.app')
@section('content')
<div class="container">
	<p>Manage Teachers</p>
	<div class="row">
		<form role="form" method="POST" action="{{url('/admin/users/update')}}">
			<div class="form-group">
				<label for="name">Username:</label>
				<input type="text" name="user_username" class="form-control" id="name" placeholder="Enter Username" value="{{ $user->username }}">
			</div>
			<div class="form-group">
				<label for="name">Password:</label>
				<input type="password" name="user_password" class="form-control" id="name" placeholder="Enter Password">
			</div>
			<div class="form-group">
				<label for="name">First Name:</label>
				<input type="text" name="user_fname" class="form-control" id="name" placeholder="Enter First Name" value="{{ $user->firstname }}">
			</div>
			<div class="form-group">
				<label for="name">Middle Name:</label>
				<input type="text" name="user_mname" class="form-control" id="name" placeholder="Enter Middle Name" value="{{ $user->middlename }}">
			</div>
			<div class="form-group">
				<label for="name">Last Name:</label>
				<input type="text" name="user_lname" class="form-control" id="name" placeholder="Enter Last Name" value="{{ $user->lastname }}" >
			</div>
			<div class="form-group">
				<label for="user_userlvl">User Level:</label>
				<select class="form-control" name="user_userlvl" id="user_userlvl" onchange="teacherConnect()">
					@foreach($userlevels as $userlevel)
					<option value=" {{ $userlevel->id }}" {{ $user->userlevel->id==$userlevel->id ? 'selected':null }}>{{ $userlevel->level_name }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group" id="teacherConnectInput">
				<label for="name">Teacher to be assigned:</label>
				<select class="form-control" name="teacher_id" id="teacher_id">
					<option value="0"> None </option>
					@foreach($teachers as $teacher)
					<option value=" {{ $teacher->id }}" {{ $user->teacher_id==$teacher->id ? 'selected':null }}>{{ $teacher->fname." ".$teacher->mname." ".$teacher->lname }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label for="name">Status:</label>
				<select class="form-control" name="status" id="name">
					@if($user->status == 1)
					<option value="1">Active</option>
					@else
					<option value="0">Inactive</option>
					@endif
				</select>
			</div>
			<input type="hidden" value="{{$user->id}}" name="user_id">
			<input type="hidden" value="{{Session::token()}}" name="_token">
			<button type="submit" class="btn btn-default">Edit Teacher</button>
		</form>
	</div>
</div>
@endsection

@section('script')
	<script type="text/javascript">
		$("#teacherConnectInput").hide();
		function teacherConnect()
		{
			var selected = $("#user_userlvl").val();
			console.log(selected);
			if(selected==3)
			{
				$("#teacherConnectInput").show();
			}
			else
			{
				$("#teacher_id").val("0");
				$("#teacherConnectInput").hide();
			}
		}
	</script>
@endsection