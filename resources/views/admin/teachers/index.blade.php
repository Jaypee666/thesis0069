@extends('layouts.app')
@section('content')
<div class="container">
	<p>Manage Teachers</p>
	<div class="row">
		@if(Session::has('success'))
		<div class="alert alert-success">
			<strong>Success: </strong> {{ Session::get('success') }}
		</div>
		@elseif(Session::has('warning'))
		<div class="alert alert-warning">
			<strong>Warning: </strong> {{ Session::get('warning') }}
		</div>
		@endif
		@if(count($errors) > 0)
		<div class="alert alert-warning">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-md-4">
			<form role="form" method="POST" action="{{url('/admin/teachers/add')}}">
				<div class="form-group">
					<label for="name">First Name:</label>
					<input type="text" name="teacher_fname" class="form-control" id="name" placeholder="Enter First Name" value="{{ Request::old('teacher_fname')}}">
				</div>
				<div class="form-group">
					<label for="name">Last Name:</label>
					<input type="text" name="teacher_lname" class="form-control" id="name" placeholder="Enter Last Name" value="{{ Request::old('teacher_lname')}}">
				</div>
				<div class="form-group">
					<label for="name">Middle Name:</label>
					<input type="text" name="teacher_mname" class="form-control" id="name" placeholder="Enter Middle Name" value="{{ Request::old('teacher_mname')}}">
				</div>
				<input type="hidden" value="1" name="status">
				<input type="hidden" value="{{Session::token()}}" name="_token">
				<button type="submit" class="btn btn-default">Add Teacher</button>
			</form>
			<form method="POST" action="{{url('/admin/teachers/import')}}" enctype="multipart/form-data">
				<input type="hidden" value="{{Session::token()}}" name="_token">
				<div class="form-group">
						<label for="import">Import Data(Optional):</label>
						<input type="file" class="btn btn-default" name="import" id="import" onchange="form.submit();">
				</div>
			</form>
		</div>
		<div class="col-md-8">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Teacher ID</th>
						<th>Teacher Name</th>
						<th>Teacher Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($teachers as $teacher)
					<tr>
						<td>
							{{ $teacher->id }}
						</td>
						<td>
							{{ $teacher->fname." ".$teacher->mname." ".$teacher->lname }}
						</td>
						<td>
							@if($teacher->status == 1)
							Active
							@else
							Inactive
							@endif
						</td>
						<td>
							<button type="button" class=".btn-danger"> <a href="{{ url('admin/teachers/edit', ['id'=>$teacher->id]) }}">Edit</a></button>
							<button type="button" class=".btn-danger"> Delete </button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $teachers->links() }}
		</div>
	</div>
</div>
@endsection