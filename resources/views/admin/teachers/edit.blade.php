@extends('layouts.app')
@section('content')
<div class="container">
	<p>Manage Teachers</p>
	<div class="row">
		<form role="form" method="POST" action="{{url('/admin/teachers/update')}}">
			<div class="form-group">
				<label for="name">First Name:</label>
				<input type="text" name="teacher_fname" class="form-control" id="name" placeholder="Enter First Name" value="{{$teacher->fname}}">
			</div>
			<div class="form-group">
				<label for="name">Last Name:</label>
				<input type="text" name="teacher_lname" class="form-control" id="name" placeholder="Enter Last Name" value="{{$teacher->lname}}">
			</div>
			<div class="form-group">
				<label for="name">Middle Name:</label>
				<input type="text" name="teacher_mname" class="form-control" id="name" placeholder="Enter Middle Name" value="{{$teacher->mname}}">
			</div>
			<div class="form-group">
				<label for="name">Status:</label>
				<select class="form-control" name="status" id="name">
				@if($teacher->status == 1)
					<option value="1">Active</option>
				@else
					<option value="0">Inactive</option>
				@endif
				</select>
			</div>
			<input type="hidden" value="{{$teacher->id}}" name="teacher_id">
			<input type="hidden" value="{{Session::token()}}" name="_token">
			<button type="submit" class="btn btn-default">Edit Teacher</button>
		</form>
	</div>
</div>
@endsection