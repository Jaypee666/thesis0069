@extends('layouts.app')
@section('content')
<div class="container">
<p>Suspended Dates</p>
	<div class="row">
		<div class="col-md-4">
			<form role="form" method="POST" action="{{url('/admin/suspension/add')}}">
				<div class="form-group">
					<label for="dateSuspend">Date:</label>
					<input type="text" name="dateSuspend" class="form-control" id="dateSuspend">
				</div>
				<div class="form-group">
					<label for="title">Event:</label>
					<input type="text" name="title" class="form-control" id="title">
				</div>
				<input type="hidden" value="1" name="status">
				<input type="hidden" value="{{Session::token()}}" name="_token">
				<button type="submit" class="btn btn-default">Add event</button>
			</form>
		</div>
		<div class="col-md-8">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>Title</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					@foreach($dates as $date)
					<tr>
						<td>{{$date->id}}</td>
						<td>{{$date->title}}</td>
						<td>{{$date->date}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $dates->links() }}
		</div>
	</div>
</div>

@endsection

@section('script')
<script>
$( function() {
	$("#dateSuspend").datepicker();
});
</script>
@endsection