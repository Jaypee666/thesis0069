@extends('layouts.app')
@section('content')
<div class="container">
	<p>Attendance Appeal</p>
	<div class="row">
		@if(Session::has('success'))
		<div class="alert alert-success">
			<strong>Success: </strong> {{ Session::get('success') }}
		</div>
		@elseif(Session::has('warning'))
		<div class="alert alert-warning">
			<strong>Warning: </strong> {{ Session::get('warning') }}
		</div>
		@endif
		@if(count($errors) > 0)
		<div class="alert alert-warning">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Request ID</th>
						<th>Student Name</th>
						<th>Teacher Name</th>
						<th>Attendance Date</th>
						<th>Attendance Time</th>
						<th>Remarks</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($teacherRequests as $teacherRequest)
					<tr>
						<td>
							{{ $teacherRequest->id }}
						</td>
						<td>
							{{ $teacherRequest->student->fname." ".$teacherRequest->student->mname." ".$teacherRequest->student->lname }}
						</td>
						<td>
							{{ $teacherRequest->teacher->fname." ".$teacherRequest->teacher->mname." ".$teacherRequest->teacher->lname }}
						</td>
						<td>
							{{ $teacherRequest->attendance_date }}
						</td>
						<td>
							{{ $teacherRequest->attendance_time }}
						</td>
						<td>
							{{ $teacherRequest->remarks." ".$teacherRequest->description }}
						</td>
						<td>
							<form role="form" method="POST" action="{{url('/moderator/requests/attendance/update')}}">
								<input type="hidden" value="{{ $teacherRequest->student->id }}" name="student_id">
								<input type="hidden" value="{{ $teacherRequest->attendance_date }}" name="attendance_date">
								<input type="hidden" value="{{ $teacherRequest->attendance_time }}" name="attendance_time">
								<input type="hidden" value="{{ $teacherRequest->remarks }}" name="remarks">
								<input type="hidden" value="{{ $teacherRequest->description }}" name="description">
								<input type="hidden" value="{{ $teacherRequest->id }}" name="id">
								<input type="hidden" value="{{Session::token()}}" name="_token">
								<button type="submit" class="btn btn-default">Accept</button>
							</form>
							<button type="button" class="btn btn-default"> <a href="">Decline</a> </button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $teacherRequests->links() }}
		</div>
	</div>
</div>
@endsection