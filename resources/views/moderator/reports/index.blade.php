@extends('layouts.app')
@section('content')
<div class="container">
	<p>Generate Reports</p>
	<div class="row">
		<div class="col-md-6">
			<form method="GET" action="{{url('moderator/reports/StudentProfile')}}">
			<input type="hidden" value="{{Session::token()}}" name="_token">
			<h2>Individual report</h2>
				<div class="form-group">
						<label for="studnum">Student Number:</label>
						<input type="text" name="studnum" class="form-control" id="studnum">
				</div>
				<div class="form-group">
						<button type="submit">Generate</button>
				</div>
			</form>
		</div>

		<div class="col-md-6">
			<form method="POST" action="{{url('moderator/reports/ClassAttendance')}}">
			<input type="hidden" value="{{Session::token()}}" name="_token">
			<h2>Class report</h2>
				<div class="form-group">
						<label for="classroom">Classroom:</label>
						<input type="text" name="classroom" class="form-control" id="classroom">
				</div>
				<div class="form-group">
						<label for="dateFrom1">Date From:</label>
						<input type="text" name="dateFrom1" class="form-control" id="dateFrom1">
				</div>
				<div class="form-group">
						<label for="dateTo1">Date To:</label>
						<input type="text" name="dateTo1" class="form-control" id="dateTo1">
				</div>
				<div class="form-group">
						<button type="submit">Generate</button>
				</div>
			</form>
		</div>

		<div class="col-md-6">
			<form method="POST" action="{{url('moderator/reports/AttendanceSummary')}}">
			<input type="hidden" value="{{Session::token()}}" name="_token">
			<h2>Attendance Report</h2>
				<div class="form-group">
						<label for="classroom">Attendance Filter:</label>
						<select class="form-control" id="type" name="type">
							<option value="All">All</option>
							<option value="Present">Present</option>
							<option value="Absent">Absent</option>
							<option value="Tardy">Tardy</option>
						</select>
				</div>
				<div class="form-group">
						<label for="dateFrom2">Date From:</label>
						<input type="text" name="dateFrom2" class="form-control" id="dateFrom2">
				</div>
				<div class="form-group">
						<label for="dateTo2">Date To:</label>
						<input type="text" name="dateTo2" class="form-control" id="dateTo2">
				</div>
				<div class="form-group">
						<button type="submit">Generate</button>
				</div>
			</form>
		</div>

		<div class="col-md-6">
			<form method="POST" action="{{url('moderator/reports/AppealSummary')}}">
			<input type="hidden" value="{{Session::token()}}" name="_token">
			<h2>Appeal Report</h2>
				<div class="form-group">
						<label for="classroom">Attendance Filter:</label>
						<select class="form-control" id="type" name="type">
							<option value="All">All</option>
							<option value="0">Pending</option>
							<option value="1">Approved</option>
						</select>
				</div>
				<div class="form-group">
						<label for="dateFrom3">Date From:</label>
						<input type="text" name="dateFrom3" class="form-control" id="dateFrom3">
				</div>
				<div class="form-group">
						<label for="dateTo3">Date To:</label>
						<input type="text" name="dateTo3" class="form-control" id="dateTo3">
				</div>
				<div class="form-group">
						<button type="submit">Generate</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
$( function() {
	$("#dateFrom1").datepicker();
	$("#dateTo1").datepicker();
	$("#dateFrom2").datepicker();
	$("#dateTo2").datepicker();
	$("#dateFrom3").datepicker();
	$("#dateTo3").datepicker();

});
</script>
@endsection