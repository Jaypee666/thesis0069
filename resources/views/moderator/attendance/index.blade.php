@extends('layouts.app')
@section('content')
<div class="container">
	<p>Attendance Appeal</p>
	<div class="row">
		@if(Session::has('success'))
		<div class="alert alert-success">
			<strong>Success: </strong> {{ Session::get('success') }}
		</div>
		@elseif(Session::has('warning'))
		<div class="alert alert-warning">
			<strong>Warning: </strong> {{ Session::get('warning') }}
		</div>
		@endif
		@if(count($errors) > 0)
		<div class="alert alert-warning">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-md-4">
			<form role="form" method="POST" action="{{url('/moderator/attendance/add')}}">
				<div class="form-group">
					<label for="name">Student:</label>
					<select class="form-control" name="student_id" id="name">
						@foreach($students as $student)
						<option value=" {{ $student->id }}">{{ $student->fname." ".$student->mname." ".$student->lname }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="attendance_date">Attendance Date:</label>
					<input type="date" name="attendance_date">
				</div>
				<div class="form-group">
					<label for="attendance_date">Attendance Time:</label>
					<input type="time" name="attendance_time">
				</div>
				<div class="form-group">
					<label for="name">Remarks:</label>
					<select class="form-control" name="remarks" id="myvalue" onchange="teacherConnect()">
						<option value="Present">Present</option>
						<option value="Absent">Absent</option>
					</select>
				</div>
				<div class="form-group">
					<label for="name">Description:</label>
					<select class="form-control" name="description" id="mySelect">
						<option value="None"></option>
						<option value="Tardy">Tardy</option>
					</select>
				</div>
				<input type="hidden" value="1" name="status">
				<input type="hidden" value="{{Session::token()}}" name="_token">
				<button type="submit" class="btn btn-default">Add Request</button>
			</form>
		</div>
		<div class="col-md-8">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Student ID</th>
						<th>Student Name</th>
						<th>Attendance Date</th>
						<th>Attendance Time</th>
						<th>Remarks</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($classAttendances as $classAttendance)
					<tr>
						<td>
							{{ $classAttendance->student->student_number }}
						</td>
						<td>
							{{ $classAttendance->student->fname." ".$classAttendance->student->mname." ".$classAttendance->student->lname }}
						</td>
						<td>
							{{ $classAttendance->attendance_date }}
						</td>
						<td>
							{{ $classAttendance->attendance_time }}
						</td>
						<td>
							{{ $classAttendance->remarks." ".$classAttendance->description }}
						</td>
						<td>
							<button type="button" class=".btn-danger"> <a href="{{ url('moderator/attendance/edit', ['id'=>$classAttendance->id]) }}">Edit</a></button>
							<button type="button" class=".btn-danger"> <a href="">Delete</a> </button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $classAttendances->links() }}
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	function teacherConnect()
	{
		var selected = $("#myvalue").val();
		console.log(selected);
		if(selected=="Present")
		{
			$("#mySelect").empty();
			$("#mySelect").append('<option value="None"></option>');
			$("#mySelect").append('<option value="Tardy">Tardy</option>');
		}
		else
		{
			$("#mySelect").empty();
			$("#mySelect").append('<option value="Cutting" selected>Cutting</option>');
			$("#mySelect").append('<option value="Not Excused">Not Excused</option>');
			$("#mySelect").append('<option value="Excused">Excused</option>');
		}
	}
</script>
@endsection