<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('user_levels')->insert(['level_name' => 'Admin', 'status' => true]);
        DB::table('user_levels')->insert(['level_name' => 'Grade Moderator', 'status' => true]);
        DB::table('user_levels')->insert(['level_name' => 'Teacher', 'status' => true]);

        DB::table('users')->insert([
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'firstname' => 'Admin',
            'middlename' => 'Admin',
            'lastname' => 'Admin',
            'status' => true,
            'userlevel_id' => 1
        ]);

        DB::table('users')->insert([
            'username' => 'gm',
            'password' => bcrypt('gm'),
            'firstname' => 'Grade Moderator',
            'middlename' => 'Grade Moderator',
            'lastname' => 'Grade Moderator',
            'status' => true,
            'userlevel_id' => 2
        ]);

        DB::table('teachers')->insert([
            'fname' => 'Teacher',
            'lname' => 'A',
            'mname' => 'Teacher',
            'status' => true,
        ]);

        DB::table('users')->insert([
            'username' => 'teacher',
            'password' => bcrypt('teacher'),
            'firstname' => 'Teacher',
            'middlename' => 'Teacher',
            'lastname' => 'Teacher',
            'status' => true,
            'teacher_id' => 1,
            'userlevel_id' => 3
        ]);

        DB::table('sms_lookup')->insert([
            'type' => 'attendance',
            'content' => "Your student, (fullname), has been marked (remarks) as of (timestamp)",
        ]);

        DB::table('sms_lookup')->insert([
            'type' => 'appeal',
            'content' => "Your student, (fullname), has been marked (remarks) as of (timestamp)",
        ]);
    }
}
