<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('lname');
            $table->string('mname');
            $table->boolean('status');
        });

        Schema::create('classrooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('class_name');
            $table->integer('teacher_id');
            $table->boolean('status');
        });

        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student_number');
            $table->string('fname');
            $table->string('lname');
            $table->string('mname');
            $table->string('year');
            $table->string('sex');
            $table->integer('class_id');
            $table->string('guardian_fname');
            $table->string('guardian_lname');
            $table->string('guardian_mname');
            $table->string('guardian_mobile');
            $table->boolean('status');
        });

        Schema::create('user_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('level_name');
            $table->boolean('status');
        });

        Schema::create('attendance_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attendance_id');
            $table->integer('user_id');
            $table->timestamps();
        });

        Schema::create('class_attendance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->date('attendance_date');
            $table->time('attendance_time');
            $table->boolean('status');
            $table->string('remarks');
            $table->string('description');

        });

        Schema::create('teacher_request', function (Blueprint $table) {
            $table->increments('id');
            $table->date('attendance_date');
            $table->time('attendance_time');
            $table->integer('student_id');
            $table->integer('teacher_id');
            $table->boolean('status');
            $table->string('remarks');
            $table->string('description');
            $table->boolean('pending');
        });

        Schema::create('user_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->timestamps();
        });

        Schema::create('suspension_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('title');
        });

        Schema::create('sms_lookup', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('teachers');
    Schema::drop('classrooms');
        Schema::drop('students');
        Schema::drop('user_levels');
        Schema::drop('attendance_logs');
        Schema::drop('class_attendance');
        Schema::drop('teacher_request');
        Schema::drop('user_logs');
        Schema::drop('suspension_dates');
        Schema::drop('sms_lookup');
    }
}

