<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuspendedDate extends Model
{
    //
    protected $table = "suspension_dates";

    public $timestamps = false;
}
