<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Student;
use App\AttendanceLog;
use App\Classroom;
use App\ClassAttendance;
use App\TeacherRequest;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Session;
use DB;
use GuzzleHttp\Client;
use App\Sms;


class TakeAttendance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attendance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A command to take attendance of students';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
    }
}
