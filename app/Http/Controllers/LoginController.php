<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\UserLogs;


class LoginController extends Controller
{
    //
    public function getLogin()
    {
    	# code...
        if(Auth::guest()){
            return view('auth.login');
        } 
        else if (Auth::user()->userlevel_id == 1)
        {
            return redirect()->intended('/admin');
        }
        else if (Auth::user()->userlevel_id == 2)
        {
            return redirect()->intended('/moderator');
        }  
        else
        {
            return redirect()->intended('/teacher');
        }
    }

    public function postLogin(Request $request)
    {
    	# code...
    	if(Auth::attempt(['username' => $request['username'], 'password' => $request['password']])) 
        {
            $log = new UserLogs();
            $log->user_id = Auth::user()->id;
            $log->save();
            // Authentication passed...
            if (Auth::user()->userlevel_id == 1)
            {
                return redirect()->intended('/admin');
            }
            else if(Auth::user()->userlevel_id == 2)
            {
                 return redirect()->intended('/moderator');
            }
            else
            {
                return redirect()->intended('/teacher');
            }
        }
        else
        {
            return redirect()->intended('/');
        }
    }

    public function doLogout()
    {
        Auth::logout();
        return redirect()->intended('/');
    }

}
