<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TeacherRequest;
use App\ClassAttendance;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use App\Student;
use PDF;
use GuzzleHttp\Client;
use App\Sms;

class ModeratorController extends Controller
{
    //
    public function getIndex()
    {
    	return view('moderator.index');
    }

    public function getRequests()
    {
    	$teacherRequests = TeacherRequest::where([
            ['status', '=', true],
            ['pending', '=', true]
            ])->paginate(5);
    	return view ('moderator.requests.index', ['teacherRequests'=>$teacherRequests]);
    }

    public function updateRequests(Request $request)
    {
        $attendance = ClassAttendance::where([
                    ['student_id', '=',  $request->student_id],
                    ['attendance_date', '=', $request->attendance_date]
                ])->update(['remarks' => $request->remarks, 'description' => $request->description, 'attendance_time' => $request->attendance_time]);
        if($attendance == 1)
        {
            $teacherRequest = TeacherRequest::where('id', $request->id)->update(['pending' => false]);
            $student = Student::find($request->student_id);

            $sms = Sms::where("type","appeal")->first();
            $sms->content = str_replace("(fullname)", $student->fname." ".$student->mname." ".$student->lname, $sms->content);
            $sms->content = str_replace("(remarks)", $request->remarks, $sms->content);
            $sms->content = str_replace("(timestamp)", Carbon::now('Asia/Singapore'), $sms->content);
            $client = new Client();
                $result = $client->post('http://192.168.43.90:8766',['form_params' => [
                        'number' => $student->guardian_mobile,
                        'message' => $sms->content,
                    ]
                ]);
            Session::flash('success', 'updated attendance!');
        }
        else
        {
            Session::flash('warning', 'no attendance found or attendance is the same!');
        }
        return redirect('/moderator/requests');
    }

    public function getReports()
    {
        return view('moderator.reports.index');
    }

    public function postAppealReport(Request $request)
    {
        $from = Carbon::parse($request["dateFrom3"]);
        $to = Carbon::parse($request["dateTo3"]);
        $type = $request["type"];
        if($type=="All")
        {
            $logs = TeacherRequest::whereBetween('attendance_date',[$from,$to])->get();
            $data["type"] = "All";
        }
        else
        {
            $logs = TeacherRequest::whereBetween('attendance_date',[$from,$to])->where('pending',$type)->get();
            $data["type"] = ($type?"Approved":"Pending");
        }
        $data["from"] = Carbon::parse($from)->format('M d, Y');
        $data["to"] = Carbon::parse($to)->format('M d, Y');
        $data["logs"] = $logs;
        
        $pdf = view('pdf.appealReport',['data'=>$data])->render();
        return PDF::load($pdf)->show();
    }

    public function postClassReport(Request $request)
    {
        //Class report, pwedeng daily pwedeng date range, pwedeng by filter
        $from = Carbon::parse($request["dateFrom1"]);
        $to = Carbon::parse($request["dateTo1"]);
        $classroom = $request["classroom"];
        $logs = ClassAttendance::join('students','class_attendance.student_id','=','students.id')->join('classrooms','students.class_id','=','classrooms.id')->where('classrooms.class_name',$classroom)->whereBetween('attendance_date',[$from,$to])->get();
        $data["from"] = Carbon::parse($from)->format('M d, Y');
        $data["to"] = Carbon::parse($to)->format('M d, Y');
        $data["logs"] = $logs;
        $data["classroom"] = $classroom;
        $pdf = view('pdf.classReport',['data'=>$data])->render();
        return PDF::load($pdf)->show();

    }

    public function postAttendanceReport(Request $request)
    {
        $type = $request["type"];
        $from = Carbon::parse($request["dateFrom2"]);
        $to = Carbon::parse($request["dateTo2"]);
        if($type=="All")
        {  
           $logs = ClassAttendance::join('students','class_attendance.student_id','=','students.id')->join('classrooms','students.class_id','=','classrooms.id')->whereBetween('attendance_date',[$from,$to])->get();
        }
        else
        {
            $logs = ClassAttendance::join('students','class_attendance.student_id','=','students.id')->join('classrooms','students.class_id','=','classrooms.id')->where('remarks',$type)->whereBetween('attendance_date',[$from,$to])->get();
        }
        $data["from"] = Carbon::parse($from)->format('M d, Y');
        $data["to"] = Carbon::parse($to)->format('M d, Y');
        $data["logs"] = $logs;
        $data["type"] = $type;
        $pdf = view('pdf.attendanceReport',['data'=>$data])->render();
        return PDF::load($pdf)->show();
    }

    public function getDailyReport(Request $request)
    {
        
    }

    public function getStudentReport(Request $request)
    {

        $data = [];
        $student = Student::where('student_number',$request['studnum'])->first();
        $data["student_info"] = $student;
        $data["present"] = ClassAttendance::where('student_id',$student->id)->where([
            ['remarks', '=','Present'],
            ['description', '=', 'None']
            ])->get();
        $data["absent"] = ClassAttendance::where('student_id',$student->id)->where('remarks','Absent')->get();
        $data["not"] = ClassAttendance::where('student_id',$student->id)->where([
            ['remarks', '=','Absent'],
            ['description', '=', 'Not Excused']
            ])->get();
        $data["excused"] = ClassAttendance::where('student_id',$student->id)->where([
            ['remarks', '=','Absent'],
            ['description', '=', 'Excused']
            ])->get();
        $data["tardy"] = ClassAttendance::where('student_id',$student->id)->where([
            ['remarks', '=','Present'],
            ['description', '=', 'Tardy']
            ])->get();
        $data["numberOfClasses"] = count($data["present"]) + count($data["absent"]) + count($data["tardy"]);
        $pdf = view('pdf.individualReport',['data'=>$data])->render();
        return PDF::load($pdf)->show();

    }

    public function getAttendance()
    {
        $students = Student::where('status', 1)->get();
        $classAttendances = ClassAttendance::where('status', 1)->paginate(9);
        return view('moderator.attendance.index', ['classAttendances' => $classAttendances, 'students' => $students]);
    }

    public function storeAttendance(Request $request)
    {
        $ClassAttendance = ClassAttendance::where([
            ['status', '=', 1],
            ['student_id', '=', $request->student_id],
            ['attendance_date', '=', $request->attendance_date]
            ])->update(['remarks' => $request->remarks, 'attendance_time' => $request->attendance_time, 'attendance_date' => $request->attendance_date, 'status' => $request->status, 'description' => $request->description]);
        if($ClassAttendance == 1)
        {
            $student = Student::find($request->student_id);
            $sms = Sms::where("type","appeal")->first();
            $sms->content = str_replace("(fullname)", $student->fname." ".$student->mname." ".$student->lname, $sms->content);
            $sms->content = str_replace("(remarks)", $request->remarks, $sms->content);
            $sms->content = str_replace("(timestamp)", Carbon::now('Asia/Singapore'), $sms->content);

            $client = new Client();
                $result = $client->post('http://192.168.43.90:8766',['form_params' => [
                        'number' => $student->guardian_mobile,
                        'message' => $sms->content,
                    ]
                ]);
            Session::flash('success', 'updated attendance!');
        }
        else
        {
            $attendance = new ClassAttendance();
            $attendance->student_id = $request->student_id;
            $attendance->attendance_date = $request->attendance_date;
            $attendance->attendance_time = $request->attendance_time;
            $attendance->status = 1;
            $attendance->remarks = $request->remarks;
            $attendance->description = $request->description;
            if($attendance->save())
            {
                $student = Student::find($request->student_id);
                $sms = Sms::where("type","appeal")->first();
                $sms->content = str_replace("(fullname)", $student->fname." ".$student->mname." ".$student->lname, $sms->content);
                $sms->content = str_replace("(remarks)", $request->remarks, $sms->content);
                $sms->content = str_replace("(timestamp)", Carbon::now('Asia/Singapore'), $sms->content);
                $client = new Client();
                $result = $client->post('http://192.168.43.90:8766',['form_params' => [
                        'number' => $student->guardian_mobile,
                        'message' => $sms->content,
                    ]
                ]);
                Session::flash('success', 'created attendance!');
            }
            else
            {
                Session::flash('warning', 'failed to create attendance!');
            }
        }
        return redirect('/moderator/attendance');

    }

    public function editAttendance($id)
    {
        $classAttendance = ClassAttendance::find($id);
        return view('moderator.attendance.edit', ['classAttendance' => $classAttendance]);
    }

    public function updateAttendance(Request $request)
    {
        if($attendance = ClassAttendance::where('id', $request->id)->update(['remarks' => $request->remarks, 'attendance_time' => $request->attendance_time, 'attendance_date' => $request->attendance_date, 'status' => $request->status, 'description' => $request->description]))
        {
            Session::flash('success', 'updated attendance!');
        }
        else
        {
            Session::flash('warning', 'no attendance found!');
        }
        return redirect('/moderator/attendance');
    }    
}   