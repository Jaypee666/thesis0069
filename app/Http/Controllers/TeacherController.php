<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use App\Student;
use App\AttendanceLog;
use App\Classroom;
use App\ClassAttendance;
use App\TeacherRequest;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Session;
use DB;
use GuzzleHttp\Client;
use App\Sms;


class TeacherController extends Controller
{
    //
    public function getIndex()
    {
        return view('teacher.index');
    }

    public function getAttendance()
    {
        return view('teacher.attendance.index');
    }

    public function getAttendanceTake()
    {
        $json = exec("python /home/pi/pythonProject/FaceRecogV4.py http://192.168.43.14:10090/image1.jpg");
        //$data = json_decode($json);
        $data = json_decode($json,true);

        //dd($data);
        foreach($data as $face)
        {
            if($face['conf']<100)
            {
                $student = Student::find($face['id']);

                $checker = ClassAttendance::where("student_id",$student->id)->where("attendance_date",Carbon::now('Asia/Singapore')->toDateString())->get();
                if(count($checker)==0)
                {
                    //COUNT IF ALREADY MARKED WITHIN THE DAY, DON'T MARK
                    $attendance = new ClassAttendance();
                    $attendance->student_id = $student->id;
                    $attendance->attendance_date = Carbon::now();
                    $attendance->attendance_time = Carbon::now();
                    $attendance->status = 0;
                    $attendance->remarks = "Present";
                    $attendance->description = "";
                    $attendance->save();    

                    $client = new Client();
                    $sms = Sms::where("type","attendance")->first();
                    $sms->content = str_replace("(fullname)", $student->fname." ".$student->mname." ".$student->lname, $sms->content);
                    $sms->content = str_replace("(remarks)", "Present", $sms->content);
                    $sms->content = str_replace("(timestamp)", Carbon::now('Asia/Singapore'), $sms->content);
                    $result = $client->post('http://192.168.43.90:8766',['form_params' => [
                            'number' => $student->guardian_mobile,
                            'message' => $sms->content,
                        ]
                    ]);
                }
                
            }
        }
        $students = Student::get();
        //$students = Student::where('class_id',Auth::user()->teacher->classroom->id)->get();

        foreach ($students as $key => $student) {
            $checker = ClassAttendance::where("student_id",$student->id)->where("attendance_date",Carbon::now('Asia/Singapore')->toDateString())->get();
            if(count($checker)==0)
            {
                $attendance = new ClassAttendance();
                $attendance->student_id = $student->id;
                $attendance->attendance_date = Carbon::now();
                $attendance->attendance_time = Carbon::now();
                $attendance->status = 0;
                $attendance->remarks = "Absent";
                $attendance->description = "";
                $attendance->save();
                $sms = Sms::where("type","attendance")->first();
                $sms->content = str_replace("(fullname)", $student->fname." ".$student->mname." ".$student->lname, $sms->content);
                $sms->content = str_replace("(remarks)", "Present", $sms->content);
                $sms->content = str_replace("(timestamp)", Carbon::now('Asia/Singapore'), $sms->content);
                $client = new Client();
                $result = $client->post('http://192.168.43.90:8766',['form_params' => [
                        'number' => $student->guardian_mobile,
                        'message' => $sms->content,
                    ]
                ]);
            }   
        }

        return redirect('/teacher/class/attendance');
    }

    public function getRequests()
    {
        $classroom = Classroom::where('teacher_id', Auth::user()->teacher_id)->first();
        if($classroom === null)
        {
            Session::flash('warning', 'teacher not assigned to a classroom');
        } 
        else
        {
            //dd($classroom);
            $students = Student::where('class_id', $classroom->id)->get();
            $teacherRequests = TeacherRequest::where([
                ['teacher_id', '=', Auth::user()->teacher_id],
                ['attendance_date', '=', DB::raw('CURDATE()')],
                ['status', '=', 1]
                ])->paginate(5);

            return view ('teacher.requests.index', ['teacherRequests'=>$teacherRequests,'students'=>$students]);
        }
        return redirect('/teacher');

    }

    public function storeRequests(Request $request)
    {
        $this->validate($request, [
            'attendance_date' => 'required',
            'remarks' => 'required',
            'attendance_time' => 'required',
            'description' => 'required'
        ]);

        $now = Carbon::now();
        $date = $now->format('Y-m-d');

        echo $date." ".$request->attendance_date;
        if($request->attendance_date === $date)
        {
            $teacherRequest = new TeacherRequest();
            $teacherRequest->teacher_id = $request->teacher_id;
            $teacherRequest->student_id = $request->student_id;
            $teacherRequest->attendance_date = $request->attendance_date;
            $teacherRequest->status = $request->status;
            $teacherRequest->pending = $request->pending;
            $teacherRequest->remarks = $request->remarks;
            $teacherRequest->description = $request->description;
            $teacherRequest->attendance_time = $request->attendance_time;

            if($teacherRequest->save())
            {
                Session::flash('success', 'created request!');
            }
            else
            {
                Session::flash('success', 'failed to create request!');
            }
        }
        else
        {
            Session::flash('warning', 'date must be today!');
        }

        return redirect('/teacher/requests');
    }

    public function deleteRequests(Request $request)
    {
        $teacherRequest = TeacherRequest::where('id', $request->id)->update(['status' => false]);
        return redirect('/teacher/requests');
    }

    public function editRequests($id)
    {
        $classroom = Classroom::where('teacher_id', Auth::user()->teacher_id)->first();
        // $attendance = ClassAttendance::join('students', function ($join) {
        //     $join->on('class_attendance.student_id', '=', 'students.id')
        //           ->where('students.class_id', 1);
        // })->get();
        $students = Student::where('class_id', $classroom->id)->get();
        $teacherRequest = TeacherRequest::find($id);
        return view ('teacher.requests.edit', ['teacherRequest'=>$teacherRequest,'students'=>$students]);
    }

    public function updateRequests(Request $request)
    {
        $this->validate($request, [
            'attendance_date' => 'required',
            'remarks' => 'required',
            'attendance_time' => 'required',
            'description' => 'required'
        ]);

        if($teacherRequest = TeacherRequest::find($request->teacherRequest_id))
        {
            $teacherRequest->teacher_id = $request->teacher_id;
            $teacherRequest->student_id = $request->student_id;
            $teacherRequest->attendance_date = $request->attendance_date;
            $teacherRequest->attendance_time = $request->attendance_time;
            $teacherRequest->status = $request->status;
            $teacherRequest->remarks = $request->remarks;
            $teacherRequest->description = $request->description;
            $teacherRequest->save();

            Session::flash('success', 'updated request!');
        }
        else
        {
            Session::flash('warning', 'failed to update request!');
        }
        return redirect('/teacher/requests');
    }

    public function getClassAttendance()
    {
        $classrooms = Classroom::where('status', 1)->get();

        $classAttendances = ClassAttendance::join('students', 'class_attendance.student_id', '=', 'students.id')
            ->where('students.class_id', '=', $classrooms[0]->id)->get();
        return view('teacher.class.attendance.index', ['classrooms' => $classrooms, 'classAttendances' => $classAttendances]);
    }

     public function getHardcodeAttendanceTake(Request $request)
    {

        $json = exec("python /home/pi/pythonProject/FaceRecogV4.py http://192.168.43.14:10090/image1.jpg");
        //$json = exec("python /home/pi/pythonProject/FaceRecogV4.py http://192.168.43.90:8080/shot.jpg");
        // //$data = json_decode($json);
        $data = json_decode($json,true);
        //Select student where class id = current user class id
        $classid = $request["class_id"];
        $students = Student::where("class_id", $classid)->get();
        //$data = 1;
        // echo count($data);
        // dd($data);
        $attendanceCount = count($data);
        $recentLog = [];
        $currentLog = [];
        if(count($data)!=0)
        {
            for($i=0; $i< $attendanceCount; $i++)
            {
                if($attendanceCount > count($students))
                {
                    break;
                }
                $checker = ClassAttendance::where("student_id",$students[$i]->id)->where("attendance_date",Carbon::now('Asia/Singapore')->toDateString())->get();
                if(count($checker)==0)
                {
                    $attendance = new ClassAttendance();
                    $attendance->student_id = $students[$i]->id;
                    $attendance->attendance_date = Carbon::now('Asia/Singapore')->toDateString();
                    $attendance->attendance_time = Carbon::now('Asia/Singapore')->toTimeString();
                    $attendance->status = 1;
                    $attendance->remarks = "Present";
                    $attendance->description = "None";
                    $attendance->save();

                    $client = new Client();


                    $result = $client->post('http://192.168.43.90:8766',['form_params' => [
                            'number' => $students[$i]->guardian_mobile,
                            'message' => "Your student, ".$students[$i]->fname." ".$students[$i]->mname." ".$students[$i]->lname.", has been marked Present as of ".Carbon::now('Asia/Singapore'),
                        ]
                    ]);
                    array_push($recentLog, $attendance);
                }
                else
                {
                    array_push($currentLog,$checker);
                }
            }
        }

            if($attendanceCount != count($students))
            {
                if($attendanceCount > count($students))
                {
                    break;
                }
                for($i=$attendanceCount; $i<count($students); $i++)
                {
                    $checker = ClassAttendance::where("student_id",$students[$i]->id)->where("attendance_date",Carbon::now('Asia/Singapore')->toDateString())->get();
                    if(count($checker)==0)
                    {
                        $attendance = new ClassAttendance();
                        $attendance->student_id = $students[$i]->id;
                        $attendance->attendance_date = Carbon::now('Asia/Singapore')->toDateString();
                        $attendance->attendance_time = Carbon::now('Asia/Singapore')->toTimeString();
                        $attendance->status = 1;
                        $attendance->remarks = "Absent";
                        $attendance->description = "Not Excused";
                        $attendance->save();
                        $client = new Client();
                        $result = $client->post('http://192.168.43.90:8766',['form_params' => [
                                'number' => $students[$i]->guardian_mobile,
                                'message' => "Your student, ".$students[$i]->fname." ".$students[$i]->mname." ".$students[$i]->lname.", has been marked Absent as of ".Carbon::now('Asia/Singapore')
                            ]
                        ]);
                        array_push($recentLog, $attendance);
                    }
                }
            }


        if(isset($recentLog))
            $log["recent"] = $recentLog;
        if(isset($currentLog))
            $log["current"] = $currentLog;
        //dd($log["recent"][0]->student->fname);
        return redirect('/teacher/attendance')->with('data', $log);
    }

}
