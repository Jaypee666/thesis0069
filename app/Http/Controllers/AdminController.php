<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classroom;
use App\Teacher;
use App\Student;
use App\Parentt;
use App\User;
use App\UserLevel;
use App\UserLogs;
use Image;
use PDF;
use Session;
use Carbon\Carbon;
use App\SuspendedDate;
use App\Sms;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    //
    public function getIndex()
    {
        return view('admin.index');
    }

    public function getUsers()
    {
        $users = User::paginate(5);
        $teachers = Teacher::where('status', 1)->get();
        $userlevels = UserLevel::all();
        return view('admin.users.index', ['users'=>$users, 'teachers'=>$teachers, 'userlevels'=>$userlevels]);
    }

    public function storeUsers(Request $request)
    {   
        $this->validate($request, [
            'user_username' => 'required',
            'user_password' => 'required',
            'user_fname' => 'required',
            'user_mname' => 'required',
            'user_lname' => 'required'
        ]);

        $user = new User;
        $user->username = $request->user_username;
        $user->password = bcrypt($request->user_password);
        $user->firstname = $request->user_fname;
        $user->middlename = $request->user_mname;
        $user->lastname = $request->user_lname;
        $user->status = $request->status;
        $user->teacher_id = $request->teacher_id;
        $user->userlevel_id = $request->user_userlvl;

        if($user->save())
        {
            Session::flash('success', 'created user!');
        }
        else
        {
            Session::flash('warning', 'failed to create user!');
        }
        return redirect('/admin/users');
    }

    public function editUsers($id)
    {
        $user = User::find($id);
        $teachers = Teacher::where('status', 1)->get();
        $userlevels = UserLevel::all();
        return view('admin.users.edit', ['user'=>$user, 'teachers'=>$teachers, 'userlevels'=>$userlevels]);
    }

    public function updateUsers(Request $request)
    {
        if($user = User::find($request->user_id))
        {
            $user->username = $request->user_username;
            $user->password = bcrypt($request->user_password);
            $user->firstname = $request->user_fname;
            $user->middlename = $request->user_mname;
            $user->lastname = $request->user_lname;
            $user->status = $request->status;
            $user->teacher_id = $request->teacher_id;
            $user->userlevel_id = $request->user_userlvl;
            $user->save();

            Session::flash('success', 'updated user!');
        }
        else
        {
            Session::flash('warning', 'failed to update user!');
        }
        return redirect('/admin/users');
    }


    public function getTeachers()
    {
        $teachers = Teacher::paginate(5);
        return view('admin.teachers.index', ['teachers'=>$teachers]);
    }

    public function storeTeachers(Request $request)
    {
        $this->validate($request, [
            'teacher_fname' => 'required',
            'teacher_lname' => 'required',
            'teacher_mname' => 'required'
        ]);
        
        $teachers = new Teacher;
        $teachers->fname = $request->teacher_fname;
        $teachers->lname = $request->teacher_lname;
        $teachers->mname = $request->teacher_mname;
        $teachers->status = $request->status;
        
        if($teachers->save())
        {
            Session::flash('success', 'created teacher!');
        }
        else
        {
            Session::flash('warning', 'failed to create teacher!');
        }
        return redirect('/admin/teachers');

    }

    public function editTeachers($id)
    {
        $teacher = Teacher::find($id);
        return view('admin.teachers.edit', ['teacher'=>$teacher]);
    }

    public function updateTeachers(Request $request)
    {

        if($teachers = Teacher::find($request->teacher_id))
        {
            $teachers->fname = $request->teacher_fname;
            $teachers->lname = $request->teacher_lname;
            $teachers->mname = $request->teacher_mname;
            $teachers->status = $request->status;
            $teachers->save();

            Session::flash('success', 'updated teacher!');
        }
        else
        {
            Session::flash('warning', 'failed to update teacher!');
        }
        return redirect('/admin/teachers');
 
    }

    public function getClassrooms()
    {
        $classrooms = Classroom::paginate(5);
        $teachers = Teacher::where('status', 1)->get();
        return view('admin.classrooms.index', ['classrooms'=>$classrooms, 'teachers'=>$teachers]);
    }

    public function storeClassrooms(Request $request)
    {
        $this->validate($request, [
            'classroom_name' => 'required'
        ]);


        $classrooms = new Classroom;
        $classrooms->class_name = $request->classroom_name;
        $classrooms->teacher_id = $request->teacher_id;
        $classrooms->status = $request->status;

        if($classrooms->save())
        {
            Session::flash('success', 'created classroom!');
        }
        else
        {
            Session::flash('warning', 'failed to create classroom!');
        }
        return redirect('/admin/classrooms');
    }

    public function editClassrooms($id)
    {
        $classroom = Classroom::find($id);
        $teachers = Teacher::all();
        return view('admin.classrooms.edit', ['classroom'=>$classroom, 'teachers'=>$teachers]);
    }

    public function updateClassrooms(Request $request)
    {
        if($classrooms = Classroom::find($request->classroom_id))
        {
            $classrooms->class_name = $request->classroom_name;
            $classrooms->teacher_id = $request->teacher_id;
            $classrooms->status = $request->status;
            $classrooms->save();

            Session::flash('success', 'updated classroom!');
        }
        else
        {
            Session::flash('warning', 'failed to update classroom!');
        }
        return redirect('/admin/classrooms');
    }


    public function getStudents()
    {
        $students = Student::paginate(5);
        $classrooms = Classroom::all();
        return view('admin.students.index', ['students'=>$students, 'classrooms'=>$classrooms]);
    }

    public function storeStudents(Request $request)
    {
        $this->validate($request, [
            'student_number' => 'required',
            'student_fname' => 'required',
            'student_lname' => 'required',
            'student_mname' => 'required',
            'year' => 'required',
            'sex' => 'required',
            'guardian_fname' => 'required',
            'guardian_lname' => 'required',
            'guardian_mname' => 'required',
            'guardian_mobile' => 'required'
        ]);

        $students = new Student;
        $students->student_number = $request->student_number;
        $students->fname = $request->student_fname;
        $students->lname = $request->student_lname;
        $students->mname = $request->student_mname;
        $students->year = $request->year;
        $students->sex = $request->sex;
        $students->class_id = $request->class_id;
        $students->guardian_fname = $request->guardian_fname;
        $students->guardian_lname = $request->guardian_lname;
        $students->guardian_mname = $request->guardian_mname;
        $students->guardian_mobile = $request->guardian_mobile;
        $students->status = $request->status;
        
        if($students->save())
        {
            Session::flash('success', 'created student!');
        }
        else
        {
            Session::flash('warning', 'failed to create student!');
        }

        return redirect('/admin/students');
    }

    public function editStudents($id)
    {
        $student = Student::find($id);
        $classrooms = Classroom::all();
        return view('admin.students.edit', ['student'=>$student, 'classrooms'=>$classrooms]);
    }

    public function updateStudents(Request $request)
    {
        if($students = Student::find($request->student_id))
        {
            $students->fname = $request->student_fname;
            $students->lname = $request->student_lname;
            $students->mname = $request->student_mname;
            $students->year = $request->year;
            $students->sex = $request->sex;
            $students->class_id = $request->class_id;
            $students->guardian_fname = $request->guardian_fname;
            $students->guardian_lname = $request->guardian_lname;
            $students->guardian_mname = $request->guardian_mname;
            $students->guardian_mobile = $request->guardian_mobile;
            $students->status = $request->status;
            $students->save();

            Session::flash('success', 'updated student!');
        }
        else
        {
            Session::flash('warning', 'failed to update student!');
        }
        return redirect('/admin/students');

    }

    public function getFaces()
    {
        $students = Student::paginate(5);
        return view('admin.faces.index',['students'=>$students]);
    }

    public function postRegisterFace(Request $request)
    {
        // $string = explode("data:image/png;base64,",$request['image']);
        // $student = Student::where('student_number',$request['studId'])->first();
        // $image = base64_decode($string[1]);
        // $filename = 'User.'.$student['id'].'.'.'1.jpg';
        // Storage::put('dataSets/'.$filename,$image);
        //dd($student);
        Session::flash('success','Face registered to database');
        return view('admin.faces.index');
    }

    public function getUserLogs()
    {
        $logs = UserLogs::paginate(5);
        return view('admin.userlogs.index',['logs'=>$logs]);
    }


    public function ajaxFaceDetect()
    {
        //$filename = 'test.jpg';
        //$tempImage = file_get_contents('http://192.168.137.1:10090/image1.jpg');
        // $tempImage = file_get_contents('https://3.bp.blogspot.com/-W__wiaHUjwI/Vt3Grd8df0I/AAAAAAAAA78/7xqUNj8ujtY/s1600/image02.png');
        //http://192.168.137.253:8080/photo.jpg
        //http://192.168.137.253:8080/image1.jpg
        $base64_str = exec("python /home/pi/pythonProject/FaceDetectv1.py http://192.168.43.14:10090/image1.jpg");
        //$base64_str = exec("python /home/pi/pythonProject/FaceDetectv1.py http://192.168.43.90:8080/shot.jpg");
        //$image = base64_decode($base64_str);
        //$data = ["image"=> $base64_str];
        //Storage::put('dataSets/'.$filename,$image);
        json_decode($base64_str,true);

        echo $base64_str;
    }

    public function getTrainFaceRecognition()
    {
        try {
            exec("python /home/pi/pythonProject/TrainerV3.py");
            echo "Good";
            Session::flash('success','System has been trained to latest data sets');
            return redirect('admin/faces/');    
        } catch (Exception $e) {
            echo "Bad";
        }
    }

    public function postGenerateUserLogs(Request $request)
    {
        $from = Carbon::parse($request["dateFrom"]);
        $to = Carbon::parse($request["dateTo"])->addDay();
        $logs = UserLogs::whereBetween('created_at',[$from,$to])->get();
        $pdf = view('pdf.userlogs',['logs'=>$logs])->render();
        return PDF::load($pdf)->show();
    }

    public function getSuspensionList()
    {
        $dates = SuspendedDate::paginate(5);

        return view('admin.suspension.index',["dates"=>$dates]);
    }

    public function postAddSuspension(Request $request)
    {
        
        $date = new SuspendedDate();
        $date->date = Carbon::parse($request["dateSuspend"]);
        $date->title = $request["title"];
        $date->save();
        return redirect('/admin/suspension')->with("Status","Event added successfuly!");
    }

    public function importUsers(Request $request)
    {
        //dd($request->file("import"));
        $csv = array_map('str_getcsv',file($request->file("import")));
        array_shift($csv);
        //dd($csv);
        foreach ($csv as $key => $import) {
            $user = new User();
            $user->username = $import[1];
            $user->password = bcrypt($import[2]);
            $user->firstname = $import[3];
            $user->middlename = $import[4];
            $user->lastname = $import[5];
            $user->status = "0";
            $user->userlevel_id = "3";
            $user->save();
        }
        
        Session::flash('success', 'Import successful!');
        return redirect('/admin/users');
    }

    public function importTeachers(Request $request)
    {
        $csv = array_map('str_getcsv',file($request->file("import")));
        array_shift($csv);
        //dd($csv);
        foreach ($csv as $key => $import) {
            $user = new Teacher();
            $user->fname = $import[1];
            $user->mname = $import[2];
            $user->lname = $import[3];
            $user->status = "1";
            $user->save();
        }
        
        Session::flash('success', 'Import successful!');
        return redirect('/admin/teachers');
    }

    public function importClassrooms(Request $request)
    {
        $csv = array_map('str_getcsv',file($request->file("import")));
        array_shift($csv);
        //dd($csv);
        foreach ($csv as $key => $import) {
            $user = new Classroom();
            $user->class_name = $import[1];
            $user->teacher_id = "1";
            $user->status = "1";
            $user->save();
        }
        
        Session::flash('success', 'Import successful!');
        return redirect('/admin/classrooms');
    }

    public function importStudents(Request $request)
    {
        $csv = array_map('str_getcsv',file($request->file("import")));
        array_shift($csv);
        //dd($csv);
        foreach ($csv as $key => $import) {
            $student = new Student;
            $student->student_number = $import[1];
            $student->fname = $import[2];
            $student->lname = $import[3];
            $student->mname = $import[4];
            $student->year = $import[5];
            $student->sex = $import[6];
            $student->class_id = $import[7];
            $student->guardian_fname = $import[8];
            $student->guardian_lname = $import[9];
            $student->guardian_mname = $import[10];
            $student->guardian_mobile = $import[11];
            $student->status = "1";
            $student->save();
        }
        
        Session::flash('success', 'Import successful!');
        return redirect('/admin/students');
    }

    public function importFaces(Request $request)
    {
        //dd($request->file("file")->getClientOriginalName());
        $name = explode(".", $request->file("file")->getClientOriginalName())[0];
        
        $student = Student::where("student_number",$name)->first();
        //dd($student);
        if($student!=null)
        {
            $request->file("file")->storeAs("datasets","User.".$student->id.".1.jpg");
            echo "Good";
        }
        else
        {
            echo "Bad";
        }
    }
    public function facePath(Request $request)
    {
        //dd(storage_path('dataSets/User.1.1.jpg'));
        return Image::make(storage_path('app\dataSets\User.'.$request["id"].'.1.jpg'))->response();
    }

    public function getSmsList()
    {
        $messages = Sms::paginate(5);
        return view('admin.sms.index',["sms"=>$messages]);
    }

    public function getEditSms(Request $request)
    {
        $message = Sms::where('id',$request['id'])->first();
        return view('admin.sms.edit',["sms"=>$message]);
    }

    public function updateSms(Request $request)
    {
        //dd($request);
        $sms = Sms::where('id',$request["id"])->first();
        $sms->content = $request["content"];
        $sms->save();
        Session::flash('success', 'Update successful!');
        return redirect('/admin/sms');
    }
}
