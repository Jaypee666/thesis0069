<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class ModeratorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guest())
        {
            return redirect()->intended('/');
        }
        else
        {
            if (Auth::user()->userlevel_id != 2)
            {
                return redirect()->intended('/');
            }
            else
            {
                 return $next($request);
            }
        } 
    }
}
