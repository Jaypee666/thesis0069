<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','firstname', 'lastname', 'middlename', 'password','status','userlevel_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', //'remember_token',
    ];

    public function userlevel()
    {
        return $this->hasOne('App\UserLevel','id','userlevel_id');
    }

    public function teacher()
    {
        return $this->hasOne('App\Teacher','id','teacher_id');
    }
}
