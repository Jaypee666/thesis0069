<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassAttendance extends Model
{
    protected $table = "class_attendance";

    public $timestamps = false;
    
    public function student()
    {
    	return $this->hasOne('App\Student','id','student_id');
    }
}
