<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLogs extends Model
{
    //
    protected $table = "user_logs";

    protected $timestamp = true;

    public function user()
    {
    	return $this->hasOne('App\User','id','user_id');
    }
}
