<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{

	protected $fillable = [
        'class_name','teacher_id'
    ];

    protected $table = "classrooms";

    public $timestamps = false;

    public function teacher()
    {
    	return $this->hasOne('App\Teacher','id','teacher_id');
    }
}
