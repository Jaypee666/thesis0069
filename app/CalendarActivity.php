<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarActivity extends Model
{
    protected $table = "calendar_activities";

    public function attendanceschedule()
    {
    	return $this->hasMany('App\AttendanceSchedule','schedule_id','id');
    }
}
