<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AttendanceLog extends Model
{
    protected $table = "attendance_logs";

    protected $timestamp = false;

    public function classattendance()
    {
    	return $this->hasMany('App\ClassAttendance','attendance_id','id');
    }

}
