<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    //

    protected $fillable = [
        'fname','lname', 'mname'
    ];

    protected $hidden = [
        'status',
    ];


    protected $table = "teachers";

    public $timestamps = false;

    public function classroom()
    {
    	return $this->hasOne('App\Classroom','teacher_id','id');
    }
    
}
