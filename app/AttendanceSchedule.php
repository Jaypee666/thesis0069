<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceSchedule extends Model
{
    protected $table = "attendance_schedule";
}
