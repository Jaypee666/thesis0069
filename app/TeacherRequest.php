<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherRequest extends Model
{
    
    protected $fillable = [
        'attendance_id','teacher_id','status'
    ];

    protected $table = "teacher_request";

    public $timestamps = false;

    public function teacher()
    {
    	return $this->hasOne('App\Teacher','id','teacher_id');
    }

    public function student()
    {
    	return $this->hasOne('App\Student', 'id', 'student_id');
    }
}
