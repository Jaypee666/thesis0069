<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    protected $fillable = [
        'fname','lname', 'mname', 'year', 'sex', 'class_id', 'guardian_fname', 'guardian_lname', 'guardian_mname', 'guardian_mobile'
    ];


    protected $hidden = [
        'status',
    ];

    protected $table = "students";

    public $timestamps = false;

    public function classroom()
    {
    	return $this->belongsTo('App\Classroom','class_id','id');
    }
}
